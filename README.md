![Django Styleguide](./assets/logo.png)

---
**Table of contents:**
<!-- table of content -->
- [Python](#python)
    - [Introduction](#introduction)
    - [Code Layout](#code-layout)
        - [Intentation](#1-use-tab-for-indentation-do-not-use-spaces)
        - [Maximum Length](#2-keep-maximum-length-99-characters)
        - [Imports](#3-imports)
        - [Blank Lines](#blank-lines)
    - [Naming Conventions](#naming-conventions)
        - [Module](#modulespackage)
        - [Variables](#variables)
        - [Functions](#functions)
        - [Classes](#classes)
    - [Comments](#comments)
    - [Recommentation](#recommendation)
    - [Tools and Tips](#toolandtip)
- [GIT](#git)
- [Rest API](#rest-api)
<!-- table of content stop -->

# Python
## Introduction

Python is one of the major programming language followed by the team at [`Ideabreed`](https://www.ideabreed.net). The purpose of this style guide is to help you understand the common conventions, standards and do and don't while writing code in python, django and django-rest-framework.

**Notes:**

1. This is stripped down version of [`PEP8`](https://peps.python.org/pep-0008/), [`HackSoftware's Django Style Guide`](https://github.com/HackSoftware/Django-Styleguide), [`Django Coding Style`](https://docs.djangoproject.com/en/dev/internals/contributing/writing-code/coding-style/) and many other resources.
1. It may differ from the common convention.
1. This is in it's early stage, some stuctures and patterns may not have been well tested and may change in future.
1. The goal is consistency.

## Code Layout

This code layout derives from [PEP8](https://peps.python.org/pep-0008/#code-lay-out) with few modification. Keep in mind, the purpose of the this code layout is to increase readability.

`“Code is read much more often than it is written.” - Guido van Rossum`

#### **1. Use tab for indentation (Do not use spaces)**

#### **2. Keep maximum length 99 characters.**

Diverges from conventional 79 characters but 99 characters in modern screens do not require horizontal scroll.

#### **3 Imports**

1. Avoid using `import *`
1. Imports should usually be on separate lines

```python
# Correct:
import os
import sys
```

```python
# Wrong:
import sys, os
```

3. Standard library should be imported at the top, followed by related library import then local imports.

When we’re working on a Django project with Django Rest Framework, imports look something like the following (excluding comments):

```python
# Standard lib imports
import os
from math import sqrt
# Core Django imports
from django.db import models
from django.utils.translation import gettext_lazy as _
# Core rest framework
from rest_framework.exceptions import APIException
# Third-party app imports
from django_extensions.db.models import TimeStampedModel
# Imports from your apps
from product.models import Stock
```
4. Do not use relative names in imports. Even if the module is in the same package, use the full package name. 
5.  Use parentheses for enclosing long lists of imports.
```python
# absolute import
from inventory.models import ProductModel
# long imports
from inventory.views import (
    InventoryViewSet
    ProductViewSet,
    StockViewSet,
    ...
)
```
### **Blank Lines**
1. Surround top-level functions and classes with two blank lines
```python
class Foo:
    ...


class Bar:
    pass


def baz():
    ...

```
2. Surround method definitions inside classes with a single blank line
```python
class TopClass:
    def first_method(self):
        ...

    def second_method(self):
        ...

```
3. Use blank lines sparingly inside functions to show clear steps.
```python
def calculate_variance(number_list):
    sum_list = 0
    for number in number_list:
        sum_list = sum_list + number
    mean = sum_list / len(number_list)

    sum_squares = 0
    for number in number_list:
        sum_squares = sum_squares + number**2
    mean_squares = sum_squares / len(number_list)

    return mean_squares - mean**2

```

## Naming Conventions
Always decide whether a class’s methods and instance variables (collectively: “attributes”) should be public or non-public. If in doubt, choose non-public; it’s easier to make it public later than to make a public attribute non-public.
### **General**
1. Name should be desciptive and should not be abreviated by deleting letters within a word (corrupted spelling).
1. If your public attribute name collides with a reserved keyword, append a single trailing underscore to your attribute name.

### **Modules/Package**
1. Modules & Packages should be named using snake pattern (`snake_pattern`).
1. In general, the name should be plural or a collective noun. 
```bash
purchase_services
|--__init__.py
|--validators.py
|--refund_processors.py
```
### **Variables**
1. Global constanst's name should use all upppercase letters.
1. Global variable's name local to the module (Internal) should start with single underscore `_`.
1. Other variable's name should follow snake pattern.
1. Private variable's name of the class should start with double underscore `__` or dunder.
```python
# global
SECRET_KET: str = os.environ.get("SECREAT_KEY")
DEFAULT_HASHING_ALGORITHM_DEPRECATED_MSG: str = ...
# internal
_TEST_SECRET_KEY:str = "xxy-k899vkw"
# private
class KeyGenerator:
    __factor = 1.22 # not to be used by its subclasses or intances.
```

### **Functions**
1. Function names should be a verb.
```python
def fetch_data(url: str) -> dict: ...

def reset_connection(connection: str) -> None: ...
```
2. Functions should follow snake pattern.
2. Declare argument and return types whenever possible.
```python
def greeting(name: str) -> str:
    return 'Hello ' + name
```
4. Use one leading underscore only for non-public methods and instance variables.
```python
def _is_token_valid(token:str) -> bool:
    ...
``` 
5. To avoid name clashes with subclasses, use two leading underscores to invoke Python’s name mangling rules.


### **Classes**
1. Classes should follow Pascal pattern.
```python
class PascalPattern:
    ... 
``` 


## Comments
`Comments that contradict the code are worse than no comments. Always make a priority of keeping the comments up-to-date when the code changes! -` [PEP8](https://peps.python.org/pep-0008/#comments).
1. Use inline comments to explain a single statement in a piece of code. 
1. Indent block comments to the same level as the code they describe.
```python
def solve_quadratic(*args) -> tuple:
    ...
    return x_a, x_b # quadratic equations have two solutions
```
3. Use document string to explain specific block of code. ([PEP257](https://peps.python.org/pep-0257/))
```python
def solve_quadratic(a, b, c, x) -> tuple:
    """
    Calculate the solution to a quadratic equation 
    using the quadratic formula.
    """
    ...
```

## Recommendations
1. Comparisons to singletons like None should always be done with is or is not, never the equality operators.
```python
if x == None # wrong

if x is None # correct
```
1. Use is not operator rather than not ... is.
```python
# Correct:
if foo is not None:

# Wrong:
if not foo is None:
```
3. Don’t compare Boolean values to True or False using the equivalence operator.
```python
# not recommended
if my_bool == True: 
    return '6 is bigger than 5'

# recommended
if my_bool: 
    return '6 is bigger than 5'

```
---
# Contributing

When contributing to this repository, please first discuss the change you wish to make via issue,
email, or any other method with the owners of this repository before making a change. 

Please note we have a code of conduct, please follow it in all your interactions with the project.

## Pull Request Process

1. Ensure any install or build dependencies are removed before the end of the layer when doing a 
   build.
2. Update the README.md with details of changes to the interface, this includes new environment 
   variables, exposed ports, useful file locations and container parameters.
3. Increase the version numbers in any examples files and the README.md to the new version that this
   Pull Request would represent. The versioning scheme we use is [SemVer](http://semver.org/).
4. You may merge the Pull Request in once you have the sign-off of two other developers, or if you 
   do not have permission to do that, you may request the second reviewer to merge it for you.
